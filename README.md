# **Application Flutter réalisée dans le cadre du cours d’Édouard Marquez**
- **REN-DEVE503-Développement environnement mobile avec utilisation d'un framework**
- **REN-DEVE504-Atelier Développement mobile**

# Auteurs
Valentin, Samuel
EPSI 2021-2022metallica

# Fonctionnalités
- Affichage d’un top 50 des meilleurs titres et albums.
- Recherche d’un artiste
- Enregistrement et affichage des albums ou titres favoris de l’utilisateur
- Affichage du détail d’un album et d’un artiste
- L’application utilise l’api «TheAudioDB»

# Installation
- Penser à installer les dépendances du fichier pubspec.yaml